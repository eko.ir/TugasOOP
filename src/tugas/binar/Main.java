package tugas.binar;

import tugas.binar.Entitas.Cat;
import tugas.binar.Entitas.Chicken;
import tugas.binar.Entitas.Fish;

public class Main {
    public static void main(String[] args) {

        Fish fish = new Fish();
        fish.setAlatBernapas("Insang");
        fish.setKaki(0);
        fish.setHabitat("Air Tawar");

        Cat cat = new Cat();
        cat.setAlatBernapas("Paru-paru");
        cat.setKaki(4);

        Chicken chicken = new Chicken();
        chicken.setAlatBernapas("Paru-paru");
        chicken.setKaki(2);

        System.out.println("Ayam bernapas dengan " + chicken.getAlatBernapas() + " dan memiliki " + chicken.getKaki() + " kaki");
        System.out.println("Kucing bernapas dengan " + cat.getAlatBernapas() + " dan memiliki " + cat.getKaki() + " kaki");
        System.out.println("Ikan bernapas dengan " + fish.getAlatBernapas() + " dan memiliki " + fish.getKaki() + " kaki serta habitatnya di " + fish.getHabitat());


    }
}
