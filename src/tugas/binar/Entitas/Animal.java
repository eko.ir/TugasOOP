package tugas.binar.Entitas;

public class Animal {
    private int kaki;
    private String alatBernapas;

    public int getKaki() {
        return kaki;
    }

    public void setKaki(int kaki) {
        this.kaki = kaki;
    }

    public String getAlatBernapas() {
        return alatBernapas;
    }

    public void setAlatBernapas(String alatBernapas) {
        this.alatBernapas = alatBernapas;
    }
}
