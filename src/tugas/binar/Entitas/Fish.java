package tugas.binar.Entitas;

public class Fish extends Animal {

    private String habitat;

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

}
